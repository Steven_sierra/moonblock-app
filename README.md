MOONBLOCK APP
=============
Clone this repository
---------------------
git clone https://Steven_sierra@bitbucket.org/Steven_sierra/moonblock-app.git

Install dependencies
--------------------

cd into folder

sudo yarn install

Run and enjoy
-------------
sudo yarn start
