import React from 'react'
import { Line } from 'react-chartjs-2'

import { AppContext } from '../../App'

const Detail = ({detail, exchange}) => {
    const graph = detail.map(item => item.C )
    const labels = detail.map(item => item.TS )
    const pair = `${detail[0].S}/${detail[0].P}`

    const data = {
        labels: labels,
        datasets: [
            {
                label: `${pair} - (${exchange})`,
                fill: true,
                lineTension: 0.1,
                backgroundColor: 'rgba(75,192,192,0.4)',
                borderColor: 'rgba(75,192,192,1)',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: 'rgba(75,192,192,1)',
                pointBackgroundColor: 'red',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: graph
            }
        ]
    }
    return <div style={{padding: 20, backgroundColor: '#222'}}>
            <Line 
                data={data}
                options={{
                    maintainAspectRatio: true,
                }}
            />
        </div>
}

export default props => (<AppContext.Consumer>
                            {({detail, exchange}) => <Detail exchange={exchange} detail={detail}/>}
                        </AppContext.Consumer>)