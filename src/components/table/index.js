import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';

import { AppContext } from '../../App'
import Template from './template'

class Table extends Component{
    viewDetail = (pair) => {
        this.props.viewDetail(pair.exchange, `${pair.S}/${pair.P}`)
    }

    render(){
        const { data } = this.props
        
        return<Grid container justify='center'>
                <Grid 
                    item 
                    lg={10}
                    md={10}
                    sm={12}
                >
                    <Template 
                        data={data}
                        viewDetail={this.viewDetail}
                    />
                </Grid>
            </Grid>
    }
}

export default props => (<AppContext.Consumer>
                    {({data, viewDetail}) => <Table {...props} data={data} viewDetail={viewDetail}/>}
                </AppContext.Consumer>)