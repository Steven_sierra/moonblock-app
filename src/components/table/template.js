import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { Table,
         TableBody,
         TableCell,
         TablePagination,
         TableRow,
         Typography,
         Paper,
         Hidden } from '@material-ui/core'
import { ScaleLoader } from 'react-spinners'

import { getIcon } from '../../icons'
import EnhancedTableHead from './table-head'

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 200,
  },
  tableWrapper: {
    overflowX: 'auto',
    backgroundColor: '#222'
  },
  tableHead: {
    backgroundColor: '#1e313e',
    marginBottom: '20px',
    color: 'white'
  },
  loader: {
      margin: 0,
      transform: 'translate(50%)'
  }
});

class Template extends React.Component {
  state = { selected: [],
            data: [],
            page: 0,
            rowsPerPage: 10 }

  handleChangePage = (event, page) => this.setState({ page })

  handleChangeRowsPerPage = event => this.setState({ rowsPerPage: event.target.value })

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const { order, 
            orderBy, 
            selected, 
            rowsPerPage, 
            page } = this.state

    const  { data, 
             viewDetail, 
             classes }  = this.props

    return (
      <Paper className={classes.root}>
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <EnhancedTableHead
              numSelected={selected.length}
              className={classes.tableHead}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
            />
            <TableBody>
              {(data.length > 0) &&
                data
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map(((coin, index) => {
                    const isSelected = this.isSelected(coin.pair);
                    const exchange = Object.keys(coin.pair)
                    return coin.data.map(pair =>{
                      return (
                        <TableRow
                          tabIndex={-1}
                          key={`${index}-${pair.exchange}`}
                          style={{backgroundColor: '#011320cf', cursor: 'pointer'}}
                          hover
                          onClick={()=> viewDetail(pair)}
                        >
                          <TableCell numeric>
                            <Typography align='center' style={{color: 'white'}}>{coin.pair}</Typography>
                          </TableCell>
                          <TableCell scope="row" style={{display: 'table-cell', verticalAlign: 'middle'}}>
                            <Typography align='center' style={{color: 'white'}}>
                              {getIcon(pair.exchange)}
                              <span style={{paddingLeft: 5}}>
                                {pair.exchange.toUpperCase()}
                              </span>
                            </Typography>
                          </TableCell>
                          <Hidden only={['xs', 'sm']}>
                            <TableCell numeric>
                              <Typography align='center' style={{color: 'white'}}>{pair.V}</Typography>
                            </TableCell>
                          </Hidden>
                          <TableCell numeric>
                            <Typography align='center' style={{color: 'white'}}>{pair.C}</Typography>
                          </TableCell>
                        </TableRow>
                      );
                    })
                }))
              }
            </TableBody>
          </Table>
        </div>
        {(!data.length > 0) &&
          <div style={{backgroundColor: '#222'}}>
            <div className={classes.loader}>
              <ScaleLoader width={10} color='white'/>
            </div>
          </div>
        }
        <TablePagination
          component="div"
          className={classes.tableHead}
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          rowsPerPageOptions={[]}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    );
  }
}

Template.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Template);