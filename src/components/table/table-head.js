import React from 'react'
import { TableCell,
         TableHead,
         TableRow,
         Typography,
         Hidden } from '@material-ui/core';

const EnhancedTableHead = ({ className }) => (
        <TableHead className={className}>
          <TableRow>
            <TableCell>
              <Typography variant='subheading' align='center' style={{color: 'white'}} >
                PAIR
              </Typography>
            </TableCell>
            <TableCell>
              <Typography variant='subheading' align='center' style={{color: 'white'}} >
                EXCHANGE
              </Typography>
            </TableCell>
            <Hidden only={['xs', 'sm']}>
              <TableCell>
                <Typography variant='subheading' align='center' style={{color: 'white'}} >
                  VOLUME
                </Typography>
              </TableCell>
            </Hidden>
            <TableCell>
              <Typography variant='subheading' align='center' style={{color: 'white'}} >
                PRICE(low)
              </Typography>
            </TableCell>
          </TableRow>
        </TableHead>
    )

export default EnhancedTableHead