import React from 'react'
import { Typography,
         Hidden } from '@material-ui/core'

const Content = () =>(
    <div style={{width: '80%', margin: 'auto', paddingBottom: 15, paddingTop: 25}}>
        <Hidden smDown>
            <Typography variant='headline' style={{color: 'white'}}>
                All of the professional trading tools you need in one single platform.
                Search, analyze market information and trade in a SMART way
            </Typography>
        </Hidden>
        <Hidden only={['lg','md']}>
            <Typography variant='Subheading' style={{color: 'white'}}>
                Search, analyze market information and trade in a SMART way
            </Typography>
        </Hidden>
    </div>
)

export default Content