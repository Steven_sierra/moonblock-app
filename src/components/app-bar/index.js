import React from 'react';
import { AppBar,
         Hidden,
         Toolbar } from '@material-ui/core';
import logo from '../../logo.svg';


const SimpleAppBar = (props) => {
  return (
    <div>
      <Hidden only={['lg','md']}>
        <AppBar position="static" style={{backgroundColor: '#222'}}>
          <Toolbar>
              <img src={logo} style={{maxWidth: '250px'}} alt="logo" />
          </Toolbar>
        </AppBar>
      </Hidden>
      <Hidden smDown>
        <img src={logo} style={{maxWidth: '450px', display: 'block', margin: 'auto'}} alt="logo" />
      </Hidden>
    </div>
  );
}


export default SimpleAppBar