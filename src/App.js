import React, { Component } from 'react';
import { Dialog, Slide } from '@material-ui/core'

import Table from './components/table'
import AppBar from './components/app-bar'
import Content from './components/content'
import Details from './components/details'
import { getData, getDetail } from './request'
import './App.css';

const AppContext = React.createContext();

class App extends Component {
  state = { data: [],
            detail: [],
            exchange: '',
            mount: false,
            open: false }

  async componentDidMount(){
    this.setState({mount:true})

    const data = await getData()
    if(data) this.setState({data})
  }

  viewDetail = async (exchange, pair) => {
    const detail = await getDetail(exchange, pair)
    this.setState({ detail,
                    exchange,
                    open: true })
  }

  closeDialog = () => this.setState({open: false})

  render() {
    const { open, mount } = this.state
    return (
        <AppContext.Provider
          value={{ ...this.state, viewDetail: this.viewDetail }}
        >
          <AppBar />
          <Slide in={mount} timeout={{enter: 2000}} direction='up'>
            <Content />
          </Slide>
          <Table />
          <Dialog open={open} fullWidth onClose={this.closeDialog}>
            <Details />
          </Dialog>
        </AppContext.Provider>
    );
  }
}

export { App, AppContext };
