const getExchanges = async() => {
    const response = await fetch('http://alpha.moonblock.ai/api/exchanges');
    const json = await response.json()
    return json
}

const getPairs = async(exchange) => {
    const data = await fetch(`http://alpha.moonblock.ai/api/exchanges/${exchange}?data=symbols`)
    const json = await data.json()
    const pairs = await json.filter(pair => {
                        const btc = pair.substring(pair.indexOf('/') + 1)
                        if(btc === 'BTC') return pair
                    })
    const prices = await Promise.all(pairs.map(async pair => {
                        const price = await getLastPrices(exchange, pair)
                        return { [pair]: {...price, exchange}}
                    }))
    return prices
}

const getLastPrices = async(exchange, pair) => {
    const response= await fetch(`http://alpha.moonblock.ai/api/exchanges/${exchange}?frame=1d&symbol=${pair}`)
    const last_price = await response.json()
    return last_price[0]
}

export const getDetail = async(exchange, pair) => {
    const response= await fetch(`http://alpha.moonblock.ai/api/exchanges/${exchange}?frame=1d&symbol=${pair}`)
    const detail= await response.json()
    return detail
}

export const getData = async() => {
    let data = []
    const exchanges = await getExchanges()
    const pairs = await Promise.all(exchanges.map(async exchange => {
                        const pairs = await getPairs(exchange)
                        
                        pairs.map(pair => {
                            const key = Object.keys(pair).shift()
                            const data_exist = data.filter(item => item.pair === key) 
                            if(data_exist.length){
                                data.map(item =>{
                                    if(item.pair === key) {
                                        item.data.push(pair[key])
                                        return item.data.sort((a,b) => a.C - b.C)
                                    }
                                })
                            }else{
                                data.push({pair: key, data: [pair[key]]})
                            }
                        })
                    }))
    
    

    return data
}