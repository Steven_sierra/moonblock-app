import React from 'react'
import binance from './binance.svg';
import exmo from './exmo.svg';

const icons = { binance:  <img src={binance} style={{maxWidth: '20px'}} alt='binance-icon' />, 
                exmo: <img src={exmo} style={{maxWidth: '30px'}} alt='exmo-icon' />,
                wex: <img style={{maxWidth: '17px', height: 'auto'}} src={'https://s2.coinmarketcap.com/static/img/exchanges/32x32/2.png'} alt='wex'/>,
                gemini: <img style={{maxWidth: '17px', height: 'auto'}} src={'https://gemini.com/wp-content/uploads/2016/02/gemini_symbol_rgb.png'} alt='gemini'/>,
                kraken: <img style={{maxWidth: '17px', height: 'auto'}} src={'https://s2.coinmarketcap.com/static/img/exchanges/32x32/24.png'} alt='kraken'/>,
                gdax: <img style={{maxWidth: '17px', height: 'auto'}} src={'https://img13.androidappsapk.co/300/8/e/0/com.levionsoftware.gdax_app.png'} alt='gdax'/>,  
                gateio: <img style={{maxWidth: '17px', height: 'auto'}} src={'https://s2.coinmarketcap.com/static/img/exchanges/32x32/302.png'} alt='gateio'/>,  
                kucoin: <img style={{maxWidth: '17px', height: 'auto'}} src={'https://s2.coinmarketcap.com/static/img/exchanges/32x32/311.png'} alt='kucoin'/>,  
                twitter: <img style={{maxWidth: '17px', height: 'auto'}} src="https://png.icons8.com/color/2x/twitter.png" alt='twitter'/>,  
              }

export function getIcon(name){
    return icons[name]
}